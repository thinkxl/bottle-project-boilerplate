#!/usr/bin/env python
# bottle.py boilerplate project

from bottle import route, run, template, static_file, request
from redis import StrictRedis as Redis
from cork import Cork
import requests, xmltodict, json, webbrowser, os

@route('/')
def index():
    """if user enter / return index.html"""
    return static_file('index.html', root='./public/dev/')

@route('/<filename:path>')
def files(filename):
    """show static files"""
    return static_file(filename, root='./public/dev/')

run(host='localhost', port=8080, debug=True, reloader=True)
